export default {
    Dark: '#1e1e1e',
    Light: '#fff',
    Primary: '#dbb2ff',
    Error: '#ff0000'
}

import {Keyboard, StatusBar, StyleSheet, Text, TouchableWithoutFeedback, View} from "react-native";
import colors from "../misc/colors";
import React, {useEffect, useState} from "react";
import SearchBar from "../components/SearchBar";
import RoundIconBtn from "../components/RoundIconBtn";
import InputModel from "../components/InputModel";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {CountryModel} from "../models/country.model";

const HomeScreen = ({user}: any) => {
    const [greet, setGreet] = useState('');
    const [modalVisible, setModalVisible] = useState(false)
    const [countries, setCountries] = useState<CountryModel[]>([])

    const findGreet = () => {
        const hrs = new Date().getHours()

        if (hrs <= 12 && hrs >= 5) {
            return setGreet('Morning');
        } else if (hrs <= 17 && hrs > 12) {
            return setGreet('Afternoon');
        } else setGreet('Evening')

    }

    const findCountries = async () => {
        const result = await AsyncStorage.getItem('countries');
        console.log(result)
        if (result !== null) setCountries(JSON.parse(result));
    }

    //funksiyani eslab qolib yangilanishlardan so'ng chaqiradi
    useEffect(() => {
        findCountries()
        findGreet()
    }, []);

    const handleOnSubmit = async (code: string, nameUz: string, nameRu: string, nameEn: string) => {
        const crud = new CountryModel();
        crud._id = Date.now();
        crud.nameUz = nameUz;
        crud.nameRu = nameRu;
        crud.nameEn = nameEn
        const updatedCountries = [...countries, crud];

        setCountries(updatedCountries)
        await AsyncStorage.setItem('countries', JSON.stringify(updatedCountries))
    }

    return (
        <>
            <StatusBar barStyle='dark-content' backgroundColor={colors.Light}/>

            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>

                <View style={styles.container}>

                    <Text style={styles.header}>
                        {`Good ${greet} ${user.name}`}
                    </Text>

                    <SearchBar containerStyle={{marginVertical: 15}}/>

                    <View style={[StyleSheet.absoluteFillObject, styles.emptyHeaderContainer]}>

                        <Text style={styles.emptyHeader}>
                            Add Countries
                        </Text>

                        <RoundIconBtn
                            onPress={() => setModalVisible(true)}
                            fontAwesomeIconName='plus'
                            style={styles.addBtn}
                            size={32}
                        />

                    </View>

                </View>
            </TouchableWithoutFeedback>

            <InputModel
                visible={modalVisible}
                onClose={() => setModalVisible(false)}
                onSubmit={handleOnSubmit}
            />
        </>
    );
};

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20,
        flex: 1,
        zIndex: 1
    },
    header: {
        fontSize: 25,
        fontWeight: 'bold',
        marginTop: 15
    },
    emptyHeaderContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: -1
    },
    emptyHeader: {
        fontSize: 30,
        textTransform: 'uppercase',
        fontWeight: 'bold',
        opacity: 0.3
    },
    addBtn: {
        position: 'absolute',
        right: 25,
        bottom: 30
    }

})

export default HomeScreen

import {Dimensions, StatusBar, StyleSheet, Text, TextInput, View} from "react-native";
import colors from "../../misc/colors";
import {useState} from "react";
import RoundIconBtn from "../../components/RoundIconBtn";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default function LoginScreen({onFinish}: any) {
    const [name = '', setName] = useState()
    const handleOnChangeText = (text: any) => setName(text)

    const handleSubmit = async () =>{
        const user = { name: name}
        await AsyncStorage.setItem('user', JSON.stringify(user))
        if (onFinish) onFinish()
    }

    return (
        <>
            <StatusBar hidden />
            <View style={styles.container}>

                <Text style={styles.inputTitle}>
                    Enter your name to continue
                </Text>
                <TextInput value={name} onChangeText={handleOnChangeText} placeholder='Enter Name'
                           style={styles.textInput} />
                {name.trim().length >= 3 ?
                    <RoundIconBtn
                    fontAwesomeIconName='arrow-right'
                    onPress={handleSubmit}
                    /> : null}
            </View>
        </>
    )
}

const width = Dimensions.get('window').width - 50

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textInput: {
        borderWidth: 2,
        borderColor: colors.Primary,
        color: colors.Primary,
        width,
        height: 50,
        borderRadius: 10,
        paddingLeft: 15,
        fontSize: 15,
        marginBottom: 15
    },
    inputTitle: {
        alignSelf: 'flex-start',
        paddingLeft: 25,
        marginBottom: 5,
        opacity: 0.5
    }
})

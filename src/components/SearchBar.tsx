import {StyleSheet, TextInput, View} from "react-native";
import colors from "../misc/colors";

const SearchBar = ({containerStyle}: any) => {
    return (
        <View style={[styles.container, {...containerStyle}]}>
            <TextInput style={styles.searchBar} placeholder='Search here ..'/>
        </View>
    )
}

const styles = StyleSheet.create({
    searchBar: {
        borderWidth: 0.5,
        borderColor: colors.Primary,
        height: 40,
        borderRadius: 40,
        paddingLeft: 15,
        fontSize: 17
    },
    container: {},
})

export default SearchBar

import Icon from "react-native-vector-icons/FontAwesome5";
import colors from "../misc/colors";
import {StyleSheet} from "react-native";

const RoundIconBtn = ({fontAwesomeIconName, size, color, style, onPress}: any) => {

    // @ts-ignore
    return <Icon
        name={fontAwesomeIconName}
        size={size || 25}
        color={color || colors.Light}
        style={[styles.icon, {...style}]}
        onPress={onPress}
    />
}

const styles = StyleSheet.create({
    icon:{
        backgroundColor: colors.Primary,
        padding: 15,
        paddingHorizontal: 17.5,
        borderRadius: 50,
        elevation: 5
    }
})

export default RoundIconBtn

import React, {useState} from 'react';
import colors from "../misc/colors";
import {Keyboard, Modal, StatusBar, StyleSheet, TextInput, TouchableWithoutFeedback, View} from "react-native";
import RoundIconBtn from "./RoundIconBtn";

const InputModel = ({visible, onClose, onSubmit}: any) => {

    const [code, setCode] = useState('');
    const [nameUz, setNameUz] = useState('');
    const [nameRu, setNameRu] = useState('');
    const [nameEn, setNameEn] = useState('');

    const all = code && nameUz && nameRu && nameEn;
    const allOr = code || nameUz || nameRu || nameEn;

    const handleSubmit = () => {
        if (!all.trim()) return closeModal();
        onSubmit(code, nameUz, nameRu, nameEn);
        setCode('');
        setNameUz('');
        setNameRu('');
        setNameEn('');
        onClose();
    }

    const closeModal = () => {
        setCode('');
        setNameUz('');
        setNameRu('');
        setNameEn('');
        onClose();
    }

    const handleModalClose = () => {
        Keyboard.dismiss();
    };

    const handleOnChangeText = (text: any, valueFor: any) => {
        if (valueFor === 'code') setCode(text)
        if (valueFor === 'nameUz') setNameUz(text)
        if (valueFor === 'nameRu') setNameRu(text)
        if (valueFor === 'nameEn') setNameEn(text)
    }

    return (
        <>
            <StatusBar hidden/>
            <Modal visible={visible} animationType='fade'>

                <View style={styles.container}>

                    <TextInput placeholder='Code'
                               value={code}
                               onChangeText={(text) => handleOnChangeText(text, 'code')}
                               style={[styles.input, styles.title]}/>

                    <TextInput placeholder='NameUz'
                               value={nameUz}
                               onChangeText={(text) => handleOnChangeText(text, 'nameUz')}
                               style={[styles.input, styles.description]}/>

                    <TextInput placeholder='NameRu'
                               value={nameRu}
                               onChangeText={(text) => handleOnChangeText(text, 'nameRu')}
                               style={[styles.input, styles.description]}/>

                    <TextInput placeholder='NameEn'
                               value={nameEn}
                               onChangeText={(text) => handleOnChangeText(text, 'nameEn')}
                               style={[styles.input, styles.description]}/>

                    {/*for buttons*/}
                    <View style={styles.btnContainer}>

                        {allOr.trim() ?
                            <RoundIconBtn
                                size={30}
                                fontAwesomeIconName='times'
                                style={{paddingHorizontal: 20, marginRight: 10}}
                                onPress={closeModal}
                            /> : null}

                        <RoundIconBtn
                            size={30}
                            fontAwesomeIconName='check'
                            style={{paddingHorizontal: 15}}
                            onPress={handleSubmit}
                        />

                    </View>

                </View>

                {/*narsa yozgandan keyin lyuby joydi bossa klaviatura yo'qoladi*/}
                <TouchableWithoutFeedback onPress={handleModalClose}>
                    <View style={[styles.modalBB, StyleSheet.absoluteFillObject]}/>
                </TouchableWithoutFeedback>
            </Modal>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20,
        paddingTop: 20
    },
    input: {
        borderBottomWidth: 2,
        borderBottomColor: colors.Primary,
        fontSize: 20,
        color: colors.Dark
    },
    title: {
        height: 40,
        marginBottom: 15,
        fontWeight: 'bold'
    },
    description: {
        height: 40,
        marginBottom: 15
    },
    modalBB: {
        flex: 1,
        zIndex: -1
    },
    btnContainer: {
        flexDirection: "row",
        justifyContent: "center",
        paddingVertical: 15
    }

})

export default InputModel;

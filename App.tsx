import LoginScreen from "./src/screens/LoginScreen/LoginScreen";
import {useEffect, useState} from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import HomeScreen from "./src/screens/HomeScreen";

class User {
    name?: string;
}

export default function App() {

    // useState o'zgaruvchi uchun boshlang'ich qiymat shart emasligini anglatadi
    const [user, setUser] = useState(new User())

    const findUser = async () => {
        const result = await AsyncStorage.getItem('user');
        if (typeof result === "string") {
            setUser(JSON.parse(result))
        }
    }

    useEffect(() => {
        findUser()
    }, []);

    if (!user.name) return <LoginScreen onFinish={findUser}/>
    return <HomeScreen user={user}/>
}
